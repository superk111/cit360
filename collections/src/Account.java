import org.junit.Test;

/**
 * Created by kylebirch on 9/22/17.
 */

public class Account {
    private String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;

    public Account(){}

    public Account(String accountNumber, String accountType, String givenName, String familyName){
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;
    }

    public void setAccountNumber(){}
    public String getAccountNumber(){return this.accountNumber;}

    @Test
    public void setAccountType(){}
    public String getAccountType(){return this.accountType;}

    public void setGivenName(){}
    public String getGivenName(){return this.givenName;}

    public void setFamilyName(){}
    public String getFamilyName(){return this.familyName;}

    public String toString(){
        return this.accountNumber + "|" + this.accountType + "|" + this.givenName + "|" + this.familyName;
    }
}

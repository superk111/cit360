
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.io.FileReader;

/**
 * Created by kylebirch on 9/22/17.
 */
public class ProcessAccounts {
    public static void main(String[] args) throws FileNotFoundException, IOException{
        //create a TreeMap to hold Account objects ordered by AccountNumber;
        TreeMap<String, Account> accountList = new TreeMap<String, Account>();

        //first create a FileReader to use in the BufferedReader.
        FileReader file = new FileReader("//Users//kylebirch//Documents//360//cit360//collections//src//rawData.csv");
        //create a BufferedReader to read the .csv file one line at a time.
        BufferedReader buffer = new BufferedReader(file);

        /*
        * for each line in the .csv:
        *   -Split the line by calling the split method in the String class.
        *   -Try to find the accountNumber in the TreeMap.
        *   -If the accountNumber is not in the TreeMap, create a new Account object and store that new object in the TreeMap.
        */

        String line = "";
        String[] brokenLine;
        //continue as long as the next read() is guaranteed.
        while(buffer.ready()){
            line = buffer.readLine();
            brokenLine = line.split(",");
            boolean accountExists = false;
            //iterate through the TreeMap.
            for(Map.Entry<String, Account> acc : accountList.entrySet()){

                if(brokenLine[0].equals(acc.getValue().getAccountNumber())){
                    accountExists = true;
                }
            }
            if(!accountExists){
                accountList.put(brokenLine[0], new Account(brokenLine[0], brokenLine[1], brokenLine[2], brokenLine[3]));
            }
        }
        //Write a for-each (or while) loop that prints each Account that is in the TreeMap.
        for(Map.Entry<String, Account> acc : accountList.entrySet()){
            System.out.println("Account: " + acc.getKey() + "| obj: " + acc.getValue().toString());
        }
    }

}

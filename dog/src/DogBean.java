

/**
 * Created by kylebirch on 9/15/17.
 */
public class DogBean extends MammalBean{
    private String breed;
    private String name;

    public DogBean(){
        super();
    }

    public DogBean(int legCount, String color, double height, String breed, String name){
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String toString(){
        System.out.println("legcount=" + this.getLegCount());
        System.out.println("color=" + this.getColor());
        System.out.println("height=" + this.getHeight());
        System.out.println("breed=" + this.getBreed());
        System.out.println("name=" + this.getName());
        return "toString";
    }


}

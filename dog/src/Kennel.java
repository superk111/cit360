

/**
 * Created by kylebirch on 9/15/17.
 */
public class Kennel {
    public Kennel(){}

    public DogBean[] buildDogs(){
        DogBean[] pack = new DogBean[5];

        pack[0] = new DogBean(4, "red", 12, "terrier", "rex");
        pack[1] = new DogBean(4, "black", 16, "beagle", "max");
        pack[2] = new DogBean(4, "brown", 14, "beagle", "tux");
        pack[3] = new DogBean(4, "black", 10, "chihuahua", "rosy");
        pack[4] = new DogBean(4, "brown", 10, "terrier", "dox");
        return pack;
    }

    public void displayDogs(DogBean[] pack){
        for(int i = 0; i < 5;i++){
            pack[i].toString();
        }
    }

    public static void main(String[] args){
        Kennel paws = new Kennel();
        DogBean[] allDogs = paws.buildDogs();
        paws.displayDogs(allDogs);

    }
}

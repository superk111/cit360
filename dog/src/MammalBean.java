/**
 * Created by kylebirch on 9/15/17.
 */
public class MammalBean {
    private int legCount;
    private String color;
    private double height;

    public MammalBean(){}

    public MammalBean(int legCount, String color, double height){
        this.legCount = legCount;
        this.color = color;
        this.height = height;
    }

    public int getLegCount(){
        return this.legCount;
    }

    public void setLegCount(int legCount){
        this.legCount = legCount;
    }

    public String getColor(){
        return this.color;
    }

    public void setColor(String color){
        this.color = color;
    }

    public double getHeight(){
        return this.height;
    }

    public void setHeight(double height){
        this.height = height;
    }
}

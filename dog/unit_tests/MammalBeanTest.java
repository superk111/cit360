import org.junit.Test;

import java.util.HashSet;
import java.util.TreeMap;

import static org.junit.Assert.*;

/**
 * Created by kylebirch on 9/29/17.
 */
public class MammalBeanTest {
    @Test
    public void testObjects(){
        MammalBean mammal = new MammalBean(4, "black", 12);
        assertEquals(4, mammal.getLegCount());
        assertEquals("black", mammal.getColor());
        assertEquals(12, mammal.getHeight(), .01);

        MammalBean k = new MammalBean(2, "green", 36);
        assertEquals(2, k.getLegCount());
        assertEquals("green", k.getColor());
        assertEquals(36, k.getHeight(), .01);

        DogBean max = new DogBean(4, "chocolate", 24, "Labrador", "Max");
        assertEquals(4, max.getLegCount());
        assertEquals("chocolate", max.getColor());
        assertEquals(24, max.getHeight(), .01);
        assertEquals("Labrador", max.getBreed());
        assertEquals("Max", max.getName());

        DogBean rex = new DogBean(4, "tan", 10, "Chihuahua", "Rex");
        assertEquals(4, rex.getLegCount());
        assertEquals("tan", rex.getColor());
        assertEquals(10, rex.getHeight(), .01);
        assertEquals("Chihuahua", rex.getBreed());
        assertEquals("Rex", rex.getName());

        HashSet<MammalBean> s = new HashSet<>();
        MammalBean l = new MammalBean(4, "black", 13);
        MammalBean m = new MammalBean(2, "blue", 60);
        MammalBean n = new MammalBean(4, "orance", 30);

        s.add(l);
        s.add(m);
        s.add(n);

        //if these weren't static-declared values, we would need this assert to verify the obj type.
        assert(l instanceof MammalBean);
        assert(m instanceof MammalBean);
        assert(n instanceof MammalBean);

        assert(s.contains(l));
        assert(s.contains(m));
        assert(s.contains(n));

        s.remove(l);

        assertFalse(s.contains(l));
        assert(s.contains(m));
        assert(s.contains(n));

        TreeMap<String, DogBean> td = new TreeMap<>();
        //if these weren't static-declared values, we would need this assert to verify the obj type.
        assert(max instanceof DogBean);
        assert(rex instanceof DogBean);
        td.put(max.getName(), max);
        td.put(rex.getName(), rex);

        assert(td.containsKey(max.getName()));
        assert(td.containsKey(rex.getName()));
    }
}